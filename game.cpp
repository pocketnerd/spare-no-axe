#include "raylib.h"
#include<string>

int main()
{
    // window dimensions
    int width{800};
    int height{450};
    InitWindow(width, height, "Spare No Axe");

    SetWindowState(FLAG_VSYNC_HINT);
    while (WindowShouldClose() == false)
    {
        const float deltaTime{GetFrameTime()};
        BeginDrawing();
        ClearBackground(WHITE);
        EndDrawing();
    }
}